#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2023 Jerry Snitselaar <jsnitsel@redhat.com>

__version__ = "0.1.0"

from pykdump.API import *
from enum import Enum
import bit_manip as bm
import core_iommu as ci
import amd_iommu
import intel_iommu


def parse_args(vendor):
    import argparse
    
    parser = argparse.ArgumentParser()
    # general arguments
    parser.add_argument("-w", dest="walk", action="store_true", help="Walk io page tables for an iova within the given device's domain")
    parser.add_argument("-c", "--capabilities", dest="caps", default=False, action="store_true", help="Parse iommu capabilities registers")
    parser.add_argument("-v", "--verbose", dest="verbose", default=False, action="store_true", help="give verbose output")
    parser.add_argument("-s", dest="srch", default=False, action="store_true", help="walk valid mappings for the device and search for address")
    parser.add_argument("-d", "--dump", dest="dump", action="store_true", help="Dump Present Page Table Entries")
    parser.add_argument("-p", "--parse-pte", dest="ppte", action="store_true", help="Parse PTE")
    parser.add_argument("-i", "--irte", dest="irte", action="store_true", help="Dump Interrupt Remapping table")

    # can take either a device struct address or a pci_dev struct address
    devgroup = parser.add_mutually_exclusive_group(required=True)
    devgroup.add_argument("--device", dest="device", type=lambda x: int(x, 16), help="hex address of a device struct for device attached to iommu", metavar="PTR-ADDR")
    devgroup.add_argument("--pcidev", dest="pcidev", type=lambda x: int(x, 16), help="hex address of a pci_dev struct for device attached to iommu", metavar="PTR-ADDR")

    srchgroup = parser.add_mutually_exclusive_group(required="-s" in sys.argv)
    srchgroup.add_argument("--paddr", dest="paddr", type=lambda x: int(x, 16), help="hex physical address to search for in mappings")
    srchgroup.add_argument("--dirty", default=False, dest="dirty", action="store_true", help="Find ptes marked with dirty bit") 
    srchgroup.add_argument("--access", default=False, dest="access", action="store_true", help="Find ptes marked with access bit") 
    parser.add_argument("--pte", required="-p" in sys.argv, dest="pte", type=lambda x: int(x, 16), help="value of pte entry to parse")
    parser.add_argument("--iova", required="-w" in sys.argv, dest="iova", type=lambda x: int(x, 16), help="hex iova address to walk io page tables for")
    parser.add_argument("--size", dest="mapsz", type=lambda x: int(x, 16), help="total size (in hex) of mapping to walk starting with iova", metavar="MAPPING-SIZE")

    # intel specific arguments

    # amd specific arguments
    if vendor == 2:
        parser.add_argument("--dte", dest="dte", default=0, action="store_true", help="Parse and dump Device Table Entry (AMD)")

    return parser.parse_args()


if __name__ == '__main__':
    # TODO: someday maybe figure this out for the Arm SMMU
    arch = sys_info.MACHINE.split()[0]
    if arch != "x86_64":
        print("Only x86_64 architecture is currently supported")
        
    cpu = readSU("struct cpuinfo_x86", readSymbol("boot_cpu_data"))
    args = parse_args(cpu.x86_vendor)

    args.pageOffsetBase = readSymbol("page_offset_base")

    if args.device is not None:
        dev = readSU("struct device", args.device)
        args.pcidev = args.device - member_offset('struct pci_dev', 'dev')
    else:
        pcidev = readSU("struct pci_dev", args.pcidev)
        args.device = args.pcidev + member_offset('struct pci_dev', 'dev')
        dev = pcidev.dev

    # Intel
    if cpu.x86_vendor == 0:
        if args.caps:
            intel_iommu.caps(dev, args)
        if args.walk:
            intel_iommu.walk(dev, args)
    # AMD
    elif cpu.x86_vendor == 2:
        args.smeMeMask = readSymbol("sme_me_mask")
        args.io_pgtable_fmt = readSymbol("amd_iommu_pgtable")
        if args.caps:
            amd_iommu.caps(dev, args)
        if args.walk:
            amd_iommu.walk(dev, args)
        if args.srch:
            amd_iommu.search(dev, args)
        if args.dump:
            amd_iommu.dump_tables(dev, args)
        if args.ppte:
            amd_iommu.parse_pte(dev, args)
        if args.dte and not (args.walk or args.srch):
            amd_iommu.dump_dte_cmd(dev, args)
        if args.irte:
            amd_iommu.dump_irte(dev, args)
    else:
        print("Only AMD and Intel IOMMUs currently supported")
