# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2023 Jerry Snitselaar <jsnitsel@redhat.com>

def c_ulonglong(x):
    return x & 0xffffffffffffffff

def c_u32(x):
    return x & 0xffffffff

def sign_clear(value, loc):
    mask = gen_mask_ull(loc-1, 0)
    return c_ulonglong(c_ulonglong(value) & mask)


def sign_extend(val, loc):
    if val & bit(loc-1):
        mask = gen_mask_ull(loc-1, 0)
        b = bit(loc)
        return c_ulonglong(c_ulonglong(c_ulonglong(val + b) & mask) - b)
    else:
        return val


def gen_mask_u32(h, l):
    return c_u32((c_u32(~0) - (c_u32(1) << l) + 1) & (c_u32(~0) >> (32 - 1 - h)))


def gen_mask_ull(h, l):
    return c_ulonglong((c_ulonglong(~0) - (c_ulonglong(1) << l) + 1) & (c_ulonglong(~0) >> (64 - 1 - h)))


def bit_u32(x):
    return gen_mask_u32(x, x)


def bit(x):
    return gen_mask_ull(x, x)


def bit_mask_u32(mask_bits, shift):
    return gen_mask_u32(mask_bits + shift - 1, shift)


def bit_mask(mask_bits, shift):
    return gen_mask_ull(mask_bits + shift - 1, shift)


def get_bits(val, mask):
    return val & mask


def get_bits_shifted(val, mask, shift):
    return get_bits(val, mask) >> shift


def bits_check(val, mask):
    return get_bits(val, mask) == mask


def ffs(val):
    idx = 0
    while not val & 0x1 and idx < 64:
        val >>= 1
        idx += 1

    if idx == 64:
        return -1
    else:
        return idx


def ffz(val):
    idx = 0
    while val & 0x1 and idx < 64:
        val >>= 1
        idx += 1

    if idx == 64:
        return -1
    else:
        return idx


def check_set(val, mask, msg):
    if bits_check(val, mask):
        print(msg)


def check_set_else(val, mask, sup_msg, nosup_msg):
    if bits_check(val, mask):
        print(sup_msg)
    else:
        print(nosup_msg)

